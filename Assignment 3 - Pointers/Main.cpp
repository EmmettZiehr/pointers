
// Assignment 3:  Pointers.
// Student: Emmett J. Ziehr.
//Date: 2/6/2021.
//Assignment Status: Finished.


#include <iostream>
#include <conio.h>

using namespace std;

// TODO: Implement the "SwapIntegers" function

//Funtion Prototype
void SwapIntegers(int *pX, int *pY);

// Do not modify the main function!
int main()
{
	int first = 0;
	int second = 0;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	SwapIntegers(&first, &second);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	_getch();
	return 0;
}

void SwapIntegers(int *pX, int *pY)
{
	int pZ = *pX;
		*pX = *pY;
		*pY = pZ;
}